const webpackDevServerConfig = require('../config/webpack.dev-server.config');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const logger = require('../lib/logger');
const webpack = require('webpack');
const path = require('path');

let toStringOptions = {
	errorDetails: true,
	warnings: true,
	modules: false,
	chunks: false,
	colors: true
};

function buildStaticRouter () {
	return new Promise((resolve, reject) => {
		logger.info('Pre-compiling our static router');
		let webpackConfig = require(path.posix.resolve('./src/config/webpack.config.precompile'))();
		let compiler = webpack(webpackConfig);

		compiler.run((err, stats) => {
			if (err) { return reject(err); }
			console.log('\n' + stats.toString(toStringOptions) + '\n');
			resolve();
		});
	});
}

function compile (webpackConfig) {
	return new Promise((resolve, reject) => {
		// If we are in development mode, we don't need to pre-compile anything
		if (process.env.NODE_ENV === 'development') { return resolve(); }

		// Precompile our static router
		return buildStaticRouter().then(() => {
			// Compile our production assets
			let compiler = webpack(webpackConfig);
			logger.info('Compiling production assets');
			compiler.run((err, stats) => {
				if (err) { return reject(err); }
				console.log('\n' + stats.toString(toStringOptions) + '\n');
				resolve();
			});
		}).catch(reject);
	});
}

function middleware (webpackConfig) {
	let compiler = webpack(webpackConfig);
	return {
		webpackDevMiddleware: webpackDevMiddleware(compiler, webpackDevServerConfig),
		webpackHotMiddleware: webpackHotMiddleware(compiler)
	};
}

module.exports = {
	middleware,
	compile
};
