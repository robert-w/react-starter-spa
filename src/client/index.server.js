import { StaticRouter } from 'react-router';
import App from './app.js';
import React from 'react';

export default function ServerRouter (props) {
	return (
		<StaticRouter location={props.url} context={props.context}>
			<App />
		</StaticRouter>
	);
}
