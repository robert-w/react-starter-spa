import React from 'react';

// Import or create our default state, must not be async
export const defaultUserState = {
	loginAttempts: 0,
	user: {}
};

export const UserContext = React.createContext(defaultUserState);
