import { USER_LOGIN, USER_LOGOUT, USER_FAILED_LOGIN } from '../../constants/userConstants';
import { defaultUserState } from '../contexts/UserContext';

/**
* If the user logs in, let's save some basic information about the user so
* we can query the backend later to determine what tasks the user can perform.
* This may be able to store local tokens, permissions, username, id, etc.
* We also want to respond to failed login attempts here as well as the logout
* action.
*/
export function updateUserInfo (state = defaultUserState.user, action) {
	const { type, data } = action;
	// Save user data on a successful login
	if (type === USER_LOGIN) {
		return data;
	}
	// Reset our user object to be empty
	else if (type === USER_LOGOUT || USER_FAILED_LOGIN) {
		return {};
	}
	// Return the default state
	else {
		return state;
	}
}

/**
* Make sure to keep track of the number of failed attempts for this session
* so we can implement a lock for this session if desired
*/
export function updateLoginAttempts (state = defaultUserState.loginAttempts, action) {
	const { type } = action;
	// Increment the failed attempts by one on failed login
	if (type === USER_FAILED_LOGIN) {
		return state + 1;
	}
	// If the user logged in or out, reset the failed attempts to 0
	else if (type === USER_LOGIN || type === USER_LOGOUT) {
		return 0;
	}
	// return default for other actions
	else {
		return state;
	}
}
