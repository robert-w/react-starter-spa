import { USER_LOGIN, USER_LOGOUT, USER_FAILED_LOGIN } from '../../constants/userConstants';
import authService from '../services/authentication.service';

export function login (username, password) {
	return dispatch => {
		authService.login(username, password)
			.then(response => dispatch({ type: USER_LOGIN, data: response }))
			.catch(err => dispatch({ type: USER_FAILED_LOGIN, data: err }));
	};
}

export function logout () {
	return { type: USER_LOGOUT };
}
