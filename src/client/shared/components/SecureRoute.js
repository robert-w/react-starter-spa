import { UserContext } from '../contexts/UserContext';
import { Redirect, Route } from 'react-router-dom';
import React from 'react';

export default function SecureRoute ({ component: Component, ...rest }) {
	return (
		<UserContext.Consumer>
			{userState => (
				<Route {...rest}
					render={props =>
						// When a user logs in, we have userState.user, later we may store
						// user roles or other features so we can do more thorough access
						// control but for starers, this will work fine
						userState.user && userState.user.id
							? <Component {...props} />
							: <Redirect to={{ pathname: '/', state: { from: props.location } }} />
					}
				/>
			)}
		</UserContext.Consumer>
	);
}
