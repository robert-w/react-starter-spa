import asyncAction from '../middleware/asyncAction';
import devLogger from '../middleware/devLogger';

import {
	updateLoginAttempts,
	updateUserInfo
} from '../reducers/userReducers';

import {
	applyMiddleware,
	combineReducers,
	createStore
} from 'redux';

// Add the async middleware
const middleware = [ asyncAction ];

// Add the logger middleware only for non production
if (process.env.NODE_ENV !== 'production') {
	middleware.push( devLogger );
}

const reducers = combineReducers({
	loginAttempts: updateLoginAttempts,
	user: updateUserInfo
});

export default createStore(
	reducers,
	applyMiddleware(...middleware)
);
