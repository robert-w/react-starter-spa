import React from 'react';

export default class NotFound extends React.Component {

	render () {
		return (
			<React.Fragment>
				<span>Sorry, We don't have what you are looking for here.</span>
			</React.Fragment>
		);
	}

}
