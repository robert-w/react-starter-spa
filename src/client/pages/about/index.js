import { UserContext } from '../../shared/contexts/UserContext';
import React from 'react';

export default class About extends React.Component {

	render () {
		return (
			<UserContext.Consumer>
				{userState => (
					<span>Hello From About</span>
				)}
			</UserContext.Consumer>
		);
	}

}
