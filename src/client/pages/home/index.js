import { UserContext } from '../../shared/contexts/UserContext';
import React from 'react';

export default class Home extends React.Component {

	render () {
		return (
			<UserContext.Consumer>
				{userState => (
					<span>Hello From Home</span>
				)}
			</UserContext.Consumer>
		);
	}

}
