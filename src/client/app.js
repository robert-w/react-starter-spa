import { defaultUserState, UserContext } from './shared/contexts/UserContext';
import SecureRoute from './shared/components/SecureRoute';
import { Route, Switch, Link } from 'react-router-dom';
import UserStore from './shared/stores/userStore.js';
import routes from './routes';
import React from 'react';

import './app.scss';

let client_routes = routes.map(route => {
	let RouteComponent = route.secure ? SecureRoute : Route;
	return (
		<RouteComponent
			key={route.key}
			path={route.path}
			exact={route.exact}
			component={route.component} />
	);
});

let client_links = routes
	.filter(route => route.link && route.path)
	.map(route =>
		<Link key={route.key} to={route.path}>
			{route.link}
		</Link>
	);


export default class App extends React.Component {

	constructor (props) {
		super(props);
		this.state = defaultUserState;
	}

	componentDidMount () {
		this.unsubscribe = UserStore.subscribe(this.userStoreDidUpdate);
	}

	componentWillUnmount () {
		this.unsubscribe();
	}

	userStoreDidUpdate = () => {
		this.setState(UserStore.getState());
	};

	render () {
		return (
			<UserContext.Provider value={this.state}>
				<header>
					{client_links}
				</header>
				<main>
					<Switch>
						{client_routes}
					</Switch>
				</main>
			</UserContext.Provider>
		);
	}
}
