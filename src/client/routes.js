import Loadable from 'react-loadable';
import React from 'react';

// Page Loading Component
let Loader = props => {
	if (props.error) {
		return <div>Error: <button onClick={props.retry}>Retry</button></div>;
	} else if (props.pastDelay) {
		return <div>Loading ... </div>;
	} else {
		return null;
	}
};

// Setup our pages here
let Login = Loadable({
	loading: Loader,
	loader: () => import(/* webpackChunkName: "home" */ './pages/login/index')
});

let Home = Loadable({
	loading: Loader,
	loader: () => import(/* webpackChunkName: "home" */ './pages/home/index')
});

let About = Loadable({
	loading: Loader,
	loader: () => import(/* webpackChunkName: "about" */ './pages/about/index')
});

let NotFound = Loadable({
	loading: Loader,
	loader: () => import(/* webpackChunkName: "not-found" */ './pages/not-found/index')
});

/*
* Export them in an array so I can work with them later
* add `secure: true` for secure routes that require authentication
*/
export default [
	{
		key: 'login',
		path: '/',
		exact: true,
		link: 'Login',
		component: Login
	},
	{
		key: 'home',
		path: '/home',
		exact: true,
		link: 'Home',
		component: Home,
		secure: true
	},
	{
		key: 'about',
		path: '/about',
		link: 'About',
		component: About,
		secure: true
	},
	{
		key: 'not_found',
		component: NotFound
	}
];
