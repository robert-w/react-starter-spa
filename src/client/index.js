import { BrowserRouter } from 'react-router-dom';
import ReactDOM from 'react-dom';
import App from './app.js';
import React from 'react';

let launch = function () {
	// Use hydrate when utilizing server side rendering
	let render = process.env.SERVER_SIDE_RENDERING
		? ReactDOM.hydrate
		: ReactDOM.render;

	// Grab any initial state from the server
	let default_state = window.INITIAL_STATE || {};

	render(
		<BrowserRouter>
			<App {...default_state} />
		</BrowserRouter>,
		document.getElementById('react-root')
	);
};

if (document.readyState === 'complete') {
	launch();
} else {
	window.onload = launch;
}

// Enable HMR
if (process.env.NODE_ENV === 'development' && module && module.hot) {
	module.hot.accept('./app.js', () => {
		let HotApp = require('./app.js').default;
		ReactDOM.render(
			<BrowserRouter>
				<HotApp />
			</BrowserRouter>,
			document.getElementById('react-root')
		);
	});
}
