const assets = require('./assets');
const path = require('path');
const glob = require('glob');

function getFilePaths () {
	return {
		files: {
			routes: glob.sync(path.posix.resolve(assets.routes)),
			views: glob.sync(path.posix.resolve(assets.views))
		}
	};
}

function setupConfig () {
	// TODO: Pull this from environment specific files
	// Unsafe-eval is used for development of hot module replacement
	// It is not needed for production, so prod locals won't need that
	let baseConfig = {
		locals: {
			title: 'React-Starter',
			author: 'Robert-W <https://github.com/Robert-W>',
			keywords: 'React, React-Router, Redux',
			description: 'Starterpack for a React powered SPA.',
			contentSecurityPolicy: 'script-src \'self\' \'unsafe-eval\';style-src \'self\' \'unsafe-inline\''
		}
	};

	return Object.assign({}, baseConfig, getFilePaths());
}

module.exports = setupConfig();
