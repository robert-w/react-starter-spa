const StatsWriterPlugin = require('../plugins/stats-writer-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

module.exports = function WebpackProdConfig () {

	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	return {
		mode: env.NODE_ENV,
		entry: {
			main: path.posix.resolve('./src/client/index.js')
		},
		output: {
			path: path.posix.join(process.cwd(), 'bin', env.PUBLIC_DIRECTORY),
			publicPath: `/${env.PUBLIC_DIRECTORY}/`,
			filename: '[name].[hash].js'
		},
		module: {
			rules: [{
				test: /\.js?$/,
				loader: 'babel-loader',
				exclude: /(node_modules)/
			}, {
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader'],
				exclude: /app\.scss$/
			}, {
				test: /app\.scss$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader', 'sass-loader']
			}]
		},
		plugins: [
			new StatsWriterPlugin({ filename: path.posix.resolve('./bin/stats.json') }),
			new MiniCssExtractPlugin({ filename: 'app.[contenthash].css' }),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.DefinePlugin(webpack_env)
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					common: {
						name: 'common',
						chunks: 'all',
						minChunks: 2,
						minSize: 0
					}
				}
			}
		}
	};
};
