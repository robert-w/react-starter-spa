/**
* @name exports
* @static
* @summary location of all assets
* @description Paths for files relative to src
*/
module.exports = {
	views: 'src/server/**/views',
	routes: 'src/server/**/routes/*.js'
};
