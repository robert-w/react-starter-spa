const StatsWriterPlugin = require('../plugins/stats-writer-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

module.exports = function WebpackDevConfig () {

	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	return {
		mode: env.NODE_ENV,
		profile: true,
		entry: {
			main: [
				'webpack-hot-middleware/client',
				path.posix.resolve('./src/client/index.js')
			]
		},
		output: {
			path: path.posix.join(process.cwd(), env.PUBLIC_DIRECTORY),
			filename: '[name].[hash].js'
		},
		module: {
			rules: [{
				test: /\.js?$/,
				loader: 'babel-loader',
				exclude: /(node_modules)/
			}, {
				test: /\.scss$/,
				use: ['style-loader', 'css-loader', 'postcss-loader', 'sass-loader']
			}]
		},
		plugins: [
			new StatsWriterPlugin({ filename: path.posix.resolve('./bin/stats.json') }),
			new webpack.optimize.OccurrenceOrderPlugin(),
			new webpack.HotModuleReplacementPlugin(),
			new webpack.DefinePlugin(webpack_env)
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					common: {
						name: 'common',
						chunks: 'all',
						minChunks: 2,
						minSize: 0
					}
				}
			}
		}
	};
};
