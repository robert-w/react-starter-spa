const getEnvironmentSettings = require('./env');
const path = require('path');

let { raw: env } = getEnvironmentSettings();

module.exports = {
	contentBase: path.posix.join(process.cwd(), env.PUBLIC_DIRECTORY),
	historyApiFallback: true,
	watchContentBase: true,
	compress: true,
	hot: true,
	stats: {
		performance: true,
		modules: false,
		colors: true,
		assets: true
	}
};
