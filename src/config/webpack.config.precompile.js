const StatsWriterPlugin = require('../plugins/stats-writer-plugin');
const getEnvironmentSettings = require('./env');
const webpack = require('webpack');
const path = require('path');

module.exports = function WebpackPrecompileConfig () {

	// Grab our environment settings
	let { raw: env, webpack_env } = getEnvironmentSettings();

	return {
		mode: env.NODE_ENV,
		profile: true,
		target: 'node',
		entry: {
			server: path.posix.resolve('./src/client/index.server.js')
		},
		output: {
			path: path.posix.join(process.cwd(), 'bin/server-components'),
			filename: '[name].compiled.js',
			libraryTarget: 'commonjs2'
		},
		module: {
			rules: [{
				test: /\.js?$/,
				loader: 'babel-loader',
				exclude: /(node_modules)/
			}, {
				test: /\.scss$/,
				use: ['css-loader', 'postcss-loader', 'sass-loader']
			}]
		},
		plugins: [
			new StatsWriterPlugin({ filename: path.posix.resolve('./bin/precompiled-stats.json') }),
			new webpack.DefinePlugin(webpack_env),
			new webpack.optimize.OccurrenceOrderPlugin()
		],
		optimization: {
			splitChunks: {
				cacheGroups: {
					common: {
						name: 'common',
						chunks: 'all',
						minChunks: 2,
						minSize: 0
					}
				}
			}
		}
	};
};
