process.env.SERVER_SIDE_RENDERING = true;
process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';
process.traceDeprecation = true;

const path = require('path');

// Grab our webpack config
let config = require(path.posix.resolve('./src/config/webpack.config.prod'))();
let main = require(path.posix.resolve('./src/main'));

main(config);
