const logger = require('../lib/logger');
const nodemon = require('nodemon');

nodemon({
	ignore: ['node_modules'],
  script: 'src/scripts/develop.js',
  ext: 'js json pug',
  verbose: true,
  watch: [
    'src/server/**/*.js',
		'src/plugins/*.js',
		'src/scripts/*.js',
    'src/shared/*.js',
    'src/config/*.js',
    'src/utils/*.js',
    'src/lib/*.js',
		'src/main.js'
  ]
});

nodemon
	.on('restart', files =>
		logger.verbose(`Nodemon restarting because ${files.join(',')} changed.`)
	)
	.on('crash', () =>
		logger.error('Nodemon crashed. Waiting for changes to restart.')
	);

// Make sure the process is killed when hitting ctrl + c
process.once('SIGINT', () => {
	nodemon.once('exit', () => {
		logger.info('Exiting Nodemon.');
		process.exit();
	});
});
