const winston = require('winston');

/**
* @name exports
* @static
* @summary Logger to use for the application
*/
module.exports = winston.createLogger({
	transports: [
		new winston.transports.Console({
			level: 'debug',
			colorize: true,
			timestamp: true
		})
	]
});
