const method_override = require('method-override');
const ReactDomServer = require('react-dom/server');
const compression = require('compression');
const body_parser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const React = require('react');
const path = require('path');
const fs = require('fs');

class Server {

	constructor (props) {
		// Mixin any props we need to
		Object.assign(this, props);
		// Create a server instance
		this.app = express();
		// return self for chaining
		return this;
	}

	configureMiddleware () {
		// Enable stack traces
		this.app.set('showStackError', true);
		// Enable jsonp via res.jsonp
		this.app.set('jsonp callback', true);
		// Enable compression
		this.app.use(compression({ level: 9 }));
		// Enable body parser
		this.app.use(body_parser.urlencoded({ extended: true }));
		this.app.use(body_parser.json());
		// Enable method override to allow for put and delete
		this.app.use(method_override());

		return this;
	}

	configureLocals (locals = {}) {
		this.app.locals.title = locals.title;
		this.app.locals.author = locals.author;
		this.app.locals.keyword = locals.keyword;
		this.app.locals.description = locals.description;
		this.app.locals.contentSecurityPolicy = locals.contentSecurityPolicy;

		return this;
	}

	configureSession () {

		return this;
	}

	configurePassport () {

		return this;
	}

	configureViewEngine (engine = 'pug', views = '') {
		this.app.set('view engine', engine);
		this.app.set('views', views);

		return this;
	}

	/**
	* The following headers are turned on by default:
	* - dnsPrefetchControl (Controle browser DNS prefetching). https://helmetjs.github.io/docs/dns-prefetch-control
	* - frameguard (prevent clickjacking). https://helmetjs.github.io/docs/frameguard
	* - hidePoweredBy (remove the X-Powered-By header). https://helmetjs.github.io/docs/hide-powered-by
	* - hsts (HTTP strict transport security). https://helmetjs.github.io/docs/hsts
	* - ieNoOpen (sets X-Download-Options for IE8+). https://helmetjs.github.io/docs/ienoopen
	* - noSniff (prevent clients from sniffing MIME type). https://helmetjs.github.io/docs/dont-sniff-mimetype
	* - xssFilter (adds small XSS protections). https://helmetjs.github.io/docs/xss-filter/
	*/
	configureHelmet () {
		this.app.use(helmet({
			hsts: process.env.USE_HSTS
		}));

		return this;
	}

	connectToDatabase () {
		// this will need to change to a promise, so it can't return this
		// this may have to be async
		return this;
	}

	configurePublicPath (public_path = process.env.PUBLIC_DIRECTORY) {
		this.app.use('/public', express.static(path.posix.join('bin', public_path)));

		return this;
	}

	setPublicRoutes (routes = []) {
		routes.forEach(route => require(route)(this.app));

		return this;
	}

	setSPARoute () {
		let precompiledStatsPath = path.posix.resolve('./bin/precompiled-stats.json');
		let statsPath = path.posix.resolve('./bin/stats.json');
		let binPath = path.posix.resolve('./bin');
		let markup = '';
		let css = '';
		// This is a catch all for the SPA, you should be mindful when developing
		// not to have any public routes that match routes defined in your client
		// side router. The public routes would get invoked and block your SPA from
		// loading the correct content on a page refresh
		this.app.get('*', (req, res) => {
			// If we are in development mode, delete the cache of the stats so we can
			// get the latest files available for our development server
			if (process.env.NODE_ENV === 'development') {
				delete require.cache[require.resolve(statsPath)];
			}
			// For prodcution, we can use the cached version so we don't need to load
			// these files more than once, may be worth preloading these in the near
			// future
			else if (process.env.NODE_ENV === 'production') {
				let precompiledStats = require(precompiledStatsPath);
				let componentPath = path.posix.join(precompiledStats.outputPath, precompiledStats.assetsByChunkName.server);
				let component = require(componentPath);
				markup = ReactDomServer.renderToString(React.createElement(component.default));
			}

			// Grab our stats, this will be cached in production
			let stats = require(statsPath);
			// Parse out code paths and styles, the format of this changes from
			// development to production. The main chunk in dev is a string, in prod,
			// it is an array with the path to a css asset that needs to be inlined
			let commonSrcPath = stats.assetsByChunkName.common || '';
			let [ appCssPath, mainSrcPath ] = Array.isArray(stats.assetsByChunkName.main)
				? stats.assetsByChunkName.main
				: [null, stats.assetsByChunkName.main];

			// Grab our css file as plaintext
			if (appCssPath) {
				css = fs.readFileSync(
					path.posix.join(binPath, stats.publicPath, appCssPath),
					{ encoding: 'utf-8' }
				);
			}

			// Send back our core template
			res.status(200).render('core', {
				commonSrc: path.posix.join(stats.publicPath, commonSrcPath),
				mainSrc: path.posix.join(stats.publicPath, mainSrcPath),
				markup: markup,
				css: css
			});
		});

		return this;
	}

	setErrorRoutes () {

		return this;
	}

	listen (port = process.env.PORT, callback) {
		this.app.listen(port, callback);
		return this;
	}

}

module.exports = Server;
